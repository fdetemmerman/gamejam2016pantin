BasicGame.Game = function (game) {

	//	When a State is added to Phaser it automatically has the following properties set on it, even if they already exist:

    this.game;		//	a reference to the currently running game
    this.add;		//	used to add sprites, text, groups, etc
    this.camera;	//	a reference to the game camera
    this.cache;		//	the game cache
    this.input;		//	the global input manager (you can access this.input.keyboard, this.input.mouse, as well from it)
    this.load;		//	for preloading assets
    this.math;		//	lots of useful common math operations
    this.sound;		//	the sound manager - add a sound, play one, set-up markers, etc
    this.stage;		//	the game stage
    this.time;		//	the clock
    this.tweens;	//	the tween manager
    this.world;		//	the game world
    this.particles;	//	the particle manager
    this.physics;	//	the physics manager
    this.rnd;		//	the repeatable random number generator

    //	You can use any of these from any function within this State.
    //	But do consider them as being 'reserved words', i.e. don't create a property for your own game called "world" or you'll over-write the world reference.

    this.starSpawnTimer = 0;
    this.starSpawnFrequency = 10; // in ms
    this.stars = [];
};

BasicGame.Game.prototype = {

  init: function () {
    this.game.renderer.renderSession.roundPixels = true;
    this.physics.startSystem( Phaser.Physics.ARCADE );
  },

	create: function () {
		//	Honestly, just about anything could go here. It's YOUR game after all. Eat your heart out!

    this.startfield = this.game.add.group();
    for (var i = 0; i < 128; i++) {
      this.startfield.add( new Star( this.game ), false );
    }

    this.obstacles = this.game.add.group();
    this.obstacles.add( new Asteroid( this.game ) );

    this.player = this.game.add.group();
    this.spaceship = new Spaceship( this.game );
    this.player.add( this.spaceship );

    this.spaceship.spawn();
    console.log( this.game);
	},

	update: function ( game ) {
    this.starSpawnTimer += game._deltaTime;
    if ( this.starSpawnTimer > this.starSpawnFrequency ) {
      var starToGenerate = this.starSpawnTimer / this.starSpawnFrequency;
      for( var i = 1; i < starToGenerate; ++i ) {
        var star = this.startfield.getFirstExists( false );
        if (star) {
          star.spawn();
        }
      }
      this.starSpawnTimer = this.starSpawnTimer % this.starSpawnFrequency;
    }
	},

	quitGame: function (pointer) {
		//	Here you should destroy anything you no longer need.
		//	Stop music, delete sprites, purge caches, free resources, all that good stuff.

		//	Then let's go back to the main menu.
		this.state.start('MainMenu');

	}

};
